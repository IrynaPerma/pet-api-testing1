import { ApiRequest } from "../request";

let baseUrl: string = 'http://tasque.lol/';

export class UsersController {
      async getAllUsers() {
        const response = await new ApiRequest()
          .prefixUrl(baseUrl)
          .method("GET")
          .url("api/Users")
          .send();
        return response;
      }

      async getCurrentUserDetails() {
        const response = await new ApiRequest()
          .prefixUrl(baseUrl)
          .method("GET")
          .url("api/Users/fromToken")
          .bearerToken("eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCIsImN0eSI6IkpXVCJ9.eyJzdWIiOiJJcnluYSIsImVtYWlsIjoiaXJ5bmFAZ21haWwuY29tIiwianRpIjoiNDU5MjE4ODctODI2OS00Yjk4LTk4OGMtMDQyMzgyYjQxOTUzIiwiaWF0IjoxNjkxNTc1OTkwLCJpZCI6IjM3MDgiLCJuYmYiOjE2OTE1NzU5ODksImV4cCI6MTY5MTU4MzE4OSwiaXNzIjoiVGhyZWFkLk5FVCBXZWJBUEkiLCJhdWQiOiJodHRwczovL2xvY2FsaG9zdDo0NDM0NCJ9.srB4odTmRCMT3s4kY7cHSD9rwnx2DCbUmYNOFjQCWjQ")
          .send();
        return response;
      }

      async updateCurrentUser(userData: any) {
        const response = await new ApiRequest()
          .prefixUrl("baseUrl")
          .method("PUT")
          .url("api/Users")
          .bearerToken("eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCIsImN0eSI6IkpXVCJ9.eyJzdWIiOiJJcnluYSIsImVtYWlsIjoiaXJ5bmFAZ21haWwuY29tIiwianRpIjoiNDU5MjE4ODctODI2OS00Yjk4LTk4OGMtMDQyMzgyYjQxOTUzIiwiaWF0IjoxNjkxNTc1OTkwLCJpZCI6IjM3MDgiLCJuYmYiOjE2OTE1NzU5ODksImV4cCI6MTY5MTU4MzE4OSwiaXNzIjoiVGhyZWFkLk5FVCBXZWJBUEkiLCJhdWQiOiJodHRwczovL2xvY2FsaG9zdDo0NDM0NCJ9.srB4odTmRCMT3s4kY7cHSD9rwnx2DCbUmYNOFjQCWjQ")
          .body(userData)
          .send();
        return response;
      }

      async getUserById(userId: number) {
        const response = await new ApiRequest()
          .prefixUrl(baseUrl)
          .method("GET")
          .url(`api/Users/${userId}`)
          .send();
        return response;
      }

      async deleteUserById(userId: number) {
        const response = await new ApiRequest()
          .prefixUrl(baseUrl)
          .method("DELETE")
          .url(`api/Users/${userId}`)
          .bearerToken("eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCIsImN0eSI6IkpXVCJ9.eyJzdWIiOiJJcnluYSIsImVtYWlsIjoiaXJ5bmFAZ21haWwuY29tIiwianRpIjoiNDU5MjE4ODctODI2OS00Yjk4LTk4OGMtMDQyMzgyYjQxOTUzIiwiaWF0IjoxNjkxNTc1OTkwLCJpZCI6IjM3MDgiLCJuYmYiOjE2OTE1NzU5ODksImV4cCI6MTY5MTU4MzE4OSwiaXNzIjoiVGhyZWFkLk5FVCBXZWJBUEkiLCJhdWQiOiJodHRwczovL2xvY2FsaG9zdDo0NDM0NCJ9.srB4odTmRCMT3s4kY7cHSD9rwnx2DCbUmYNOFjQCWjQ")
          .send();
        return response;
      }
}

export class NewUsersController {
  async getCurrentUserDetails(accessToken: string) {
    const response = await new ApiRequest()
      .prefixUrl("http://example.com/")
      .method("GET")
      .url("api/Users/fromToken")
      .bearerToken(accessToken)
      .send();

    return response;
  }

  async deleteUserById(userId: number, accessToken: string) {
    const response = await new ApiRequest()
      .prefixUrl("http://example.com/")
      .method("DELETE")
      .url(`api/Users/${userId}`)
      .bearerToken(accessToken)
      .send();

    return response;
  }
}


// import { ApiRequest } from "../request";

// const baseUrl: string = global.appConfig.baseUrl;

// export class UsersController {
//     async getAllUsers() {
//         const response = await new ApiRequest()
//             .prefixUrl(baseUrl)
//             .method("GET")
//             .url(`api/Users`)
//             .send();
//         return response;
//     }

//     async getUserById(id) {
//         const response = await new ApiRequest()
//             .prefixUrl(baseUrl)
//             .method("GET")
//             .url(`api/Users/${id}`)
//             .send();
//         return response;
//     }

//     async getCurrentUser(accessToken: string) {
//         const response = await new ApiRequest()
//             .prefixUrl(baseUrl)
//             .method("GET")
//             .url(`api/Users/fromToken`)
//             .bearerToken(accessToken)
//             .send();
//         return response;
//     }
   
//     async updateUser(userData: object, accessToken: string) {
//         const response = await new ApiRequest()
//             .prefixUrl(baseUrl)
//             .method("PUT")
//             .url(`api/Users`)
//             .body(userData)
//             .bearerToken(accessToken)
//             .send();
//         return response;
//     }
// }