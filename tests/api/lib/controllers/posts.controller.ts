import { ApiRequest } from "..//request";

let baseUrl: string = 'http://tasque.lol/';

export class PostsController {
  async getAllPosts() {
    const response = await new ApiRequest()
       .prefixUrl(baseUrl)
       .method("GET")
       .url('api/Posts')
       .send();
    return response;
  }

  async createPost(data) {
    const response = await new ApiRequest()
       .prefixUrl(baseUrl)
       .method("POST")
       .url('api/Posts')
       .body(data)
       .bearerToken("eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCIsImN0eSI6IkpXVCJ9.eyJzdWIiOiJJcnluYSIsImVtYWlsIjoicGVybWlha292YUBnbWFpbC5jb20iLCJqdGkiOiI2MTc5YWU0Zi0zY2I1LTRiNjEtYTA4MC0wMzYxMTg1ZWJhOWYiLCJpYXQiOjE2OTE1NzAxNzMsImlkIjoiMzQ0OSIsIm5iZiI6MTY5MTU3MDE3MiwiZXhwIjoxNjkxNTc3MzcyLCJpc3MiOiJUaHJlYWQuTkVUIFdlYkFQSSIsImF1ZCI6Imh0dHBzOi8vbG9jYWxob3N0OjQ0MzQ0In0.iwN_fPL4gkYmU00p1DdyZQhZ5-DVWK0Yvqa74IZJlZ0")
       .send();
    return response;
  }

  async getPostById(id: number) {
    const response = await new ApiRequest()
      .prefixUrl(baseUrl)
      .method("GET")
      .url(`api/Posts/${id}`)
      .send();
    return response;
  }

  async createComment(data) {
    const response = await new ApiRequest()
      .prefixUrl(baseUrl)
      .method("POST")
      .url("api/Comments")
      .body(data)
      .bearerToken("eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCIsImN0eSI6IkpXVCJ9.eyJzdWIiOiJJcnluYSIsImVtYWlsIjoicGVybWlha292YUBnbWFpbC5jb20iLCJqdGkiOiI2MTc5YWU0Zi0zY2I1LTRiNjEtYTA4MC0wMzYxMTg1ZWJhOWYiLCJpYXQiOjE2OTE1NzAxNzMsImlkIjoiMzQ0OSIsIm5iZiI6MTY5MTU3MDE3MiwiZXhwIjoxNjkxNTc3MzcyLCJpc3MiOiJUaHJlYWQuTkVUIFdlYkFQSSIsImF1ZCI6Imh0dHBzOi8vbG9jYWxob3N0OjQ0MzQ0In0.iwN_fPL4gkYmU00p1DdyZQhZ5-DVWK0Yvqa74IZJlZ0")
      .send();
    return response;
  }

  async addReaction(data) {
    const response = await new ApiRequest()
      .prefixUrl(baseUrl)
      .method("POST")
      .url("api/Posts/like")
      .body(data)
      .bearerToken("eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCIsImN0eSI6IkpXVCJ9.eyJzdWIiOiJJcnluYSIsImVtYWlsIjoicGVybWlha292YUBnbWFpbC5jb20iLCJqdGkiOiI2MTc5YWU0Zi0zY2I1LTRiNjEtYTA4MC0wMzYxMTg1ZWJhOWYiLCJpYXQiOjE2OTE1NzAxNzMsImlkIjoiMzQ0OSIsIm5iZiI6MTY5MTU3MDE3MiwiZXhwIjoxNjkxNTc3MzcyLCJpc3MiOiJUaHJlYWQuTkVUIFdlYkFQSSIsImF1ZCI6Imh0dHBzOi8vbG9jYWxob3N0OjQ0MzQ0In0.iwN_fPL4gkYmU00p1DdyZQhZ5-DVWK0Yvqa74IZJlZ0")
      .send();
    return response;
  }



}