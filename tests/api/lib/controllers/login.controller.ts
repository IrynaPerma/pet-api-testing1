import { ApiRequest } from "../request";

let baseUrl: string = 'http://tasque.lol/';

export class LoginController {
  async registerUser(data) {
    const response = await new ApiRequest()
       .prefixUrl(baseUrl)
       .method("POST")
       .url('api/Register')
       .body(data)
       .send();
    return response;
  }

  async loginUser(data) {
    const response = await new ApiRequest()
       .prefixUrl(baseUrl)
       .method("POST")
       .url('api/Auth/login')
       .body(data)
       .send();
    return response;
  }

  async loginuser(emailValue: string, passwordValue: string) {
    const response = await new ApiRequest()
       .prefixUrl(baseUrl)
       .method("POST")
       .url('api/Auth/login')
       .body({
                email: emailValue,
                password: passwordValue,
       })
       .send();
    return response;
  }
  
}
