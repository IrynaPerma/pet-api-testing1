import { expect } from "chai";
import { UsersController } from "../lib/controllers/users.controller";
import { checkStatusCode, checkResponseTime } from "../../helpers/functionsForChecking.helper";

const users = new UsersController();

xdescribe("Get All Users", () => {
  it(`should retrieve a list of all users`, async () => {
    const response = await users.getAllUsers();

    checkStatusCode(response, 200);
    checkResponseTime(response,500);


    const responseBody = response.body;

    const userIdToCheck = 3711;

    const userWithId = responseBody.find(user => user.id === userIdToCheck);
    expect(userWithId, `User with ID ${userIdToCheck} should exist`).to.not.be.undefined;

    console.log(responseBody);
  });
});




