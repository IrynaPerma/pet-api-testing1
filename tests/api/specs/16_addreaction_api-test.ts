import { expect } from "chai";
import { PostsController } from "../lib/controllers/posts.controller";
import { checkStatusCode, checkResponseTime } from "../../helpers/functionsForChecking.helper";

const posts = new PostsController();

xdescribe("Posts controller", () => {
  it(`should add a like reaction to a post`, async () => {

    const reactionData = {
      entityId: 1633,
      isLike: true,
      userId: 0,
    };

    const response = await posts.addReaction(reactionData);

    checkStatusCode(response, 200);
    checkResponseTime(response,500);

    const responseBody = response.body;

    console.log(responseBody);
  });
});

