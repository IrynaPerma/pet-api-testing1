import { expect } from "chai";
import { UsersController } from "../lib/controllers/users.controller";
import { checkStatusCode, checkResponseTime } from "../../helpers/functionsForChecking.helper";

const users = new UsersController();

xdescribe("Users controller", () => {
  it(`should delete user by ID`, async () => {
    const userId = 3708; 

    const response = await users.deleteUserById(userId);

    checkStatusCode(response, 204);
    checkResponseTime(response,300);

  });
});







