import { expect } from "chai";
import { PostsController } from "../lib/controllers/posts.controller";
import { checkStatusCode, checkResponseTime } from "../../helpers/functionsForChecking.helper";


const posts = new PostsController();

xdescribe("Posts controller", () => {
  it(`should add a new comment to a post`, async () => {
    let commentData = {
      authorId: 0,
      postId: 1633,
      body: "Nice!",
    };

    const response = await posts.createComment(commentData);

    checkStatusCode(response, 200);
    checkResponseTime(response,500);

    expect(response.body).to.have.property("id").that.is.a("number");
    expect(response.body).to.have.property("createdAt").that.is.a("string");
    expect(response.body).to.have.property("author");
    expect(response.body).to.have.property("body").that.is.equal(commentData.body);
    expect(response.body).to.have.property("reactions").that.is.an("array");

    console.log(response.body);
  });
});
