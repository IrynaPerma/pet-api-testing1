import { expect } from "chai";
import { LoginController } from "../lib/controllers/login.controller";
import { checkStatusCode, checkResponseTime } from "../../helpers/functionsForChecking.helper";

const users = new LoginController();

xdescribe("Login controller", () => {
  it(`should register a new user`, async () => {
      let userData: object = {
          id: 0,
          avatar: "string",
          email: "iryna@gmail.com",
          userName: "Iryna",
          password: "12345678",
      };

    const response = await users.registerUser(userData);

    checkStatusCode(response, 201);
    checkResponseTime(response,500);
    
    expect(response.body).to.have.property("id").that.is.a("number");

    console.log(response.body);
  });
});



