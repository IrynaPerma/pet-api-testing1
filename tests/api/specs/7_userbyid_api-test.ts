import { expect } from "chai";
import { UsersController } from "../lib/controllers/users.controller";
import { checkStatusCode, checkResponseTime } from "../../helpers/functionsForChecking.helper";

const users = new UsersController();

xdescribe("Users controller", () => {
  it(`should retrieve user information by ID`, async () => {
    const userId = 3708; 

    const response = await users.getUserById(userId);

    checkStatusCode(response, 200);
    checkResponseTime(response,300);

    const responseBody = response.body;

    expect(responseBody).to.have.property("id").that.is.equal(userId);
    expect(responseBody).to.have.property("avatar").that.is.a("string");
    expect(responseBody).to.have.property("email").that.is.a("string");
    expect(responseBody).to.have.property("userName").that.is.a("string");
  });
});
