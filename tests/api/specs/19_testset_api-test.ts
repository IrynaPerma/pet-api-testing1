import {
  checkResponseTime,
  checkStatusCode, 
} from '../../helpers/functionsForChecking.helper';
import { LoginController } from '../lib/controllers/login.controller';
const login = new LoginController();

describe('Use test data set for login', () => {
  let invalidCredentialsDataSet = [
      { email: 'iryna.qa.test@gmail.com', password: '' },
      { email: 'iryna.qa.test@gmail.com', password: '      ' },
      { email: 'iryna.qa.test@gmail.com', password: 'ATest2023! ' },
      { email: 'iryna.qa.test@gmail.com', password: 'ATest 2021' },
      { email: 'iryna.qa.test@gmail.com', password: 'admin' },
      { email: 'iryna.qa.test@gmail.com', password: 'iryna.qa.test@gmail.com' },
  ];

  invalidCredentialsDataSet.forEach((credentials) => {
      it(`should not login using invalid credentials : '${credentials.email}' + '${credentials.password}'`, async () => {
          let response = await login.loginuser(credentials.email, credentials.password);

          checkStatusCode(response, 401); 
          checkResponseTime(response, 3000);
      });
  });
});
