import { expect } from "chai";
import { LoginController } from "../lib/controllers/login.controller";
import { checkStatusCode, checkResponseTime } from "../../helpers/functionsForChecking.helper";

const users = new LoginController();

xdescribe("Login controller", () => {
  it(`should log in a user`, async () => {
    let userData = {
      email: "permiakova@gmail.com",
      password: "12345678",
    };

    let response = await users.loginUser(userData);

    const responseBody = response.body;

    checkStatusCode(response, 200);
    checkResponseTime(response,300);

    expect(responseBody.user.userName).to.equal("Iryna");
    expect(responseBody.token.accessToken.token).to.be.a("string");
    expect(responseBody.token.accessToken.expiresIn).to.be.a("number");
    expect(responseBody.token.refreshToken).to.be.a("string");

    console.log(response.body);
  });
});
