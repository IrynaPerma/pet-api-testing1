import { expect } from "chai";
import { UsersController } from "../lib/controllers/users.controller";
import { checkStatusCode, checkResponseTime } from "../../helpers/functionsForChecking.helper";

const usersController = new UsersController();

xdescribe("Get Current User Details from Token", () => {
  it("should retrieve details of the current user who is logged in", async () => {

    const response = await usersController.getCurrentUserDetails();

    const responseBody = response.body;

    checkStatusCode(response, 201);
    checkResponseTime(response,300);
    
    expect(responseBody).to.have.property("id");
    expect(responseBody).to.have.property("avatar");
    expect(responseBody).to.have.property("email");
    expect(responseBody).to.have.property("userName");
  });
});










