import { expect } from "chai";
import { LoginController } from "../lib/controllers/login.controller";
import { NewUsersController } from "../lib/controllers/users.controller";
import { checkStatusCode, checkResponseTime } from "../../helpers/functionsForChecking.helper";

const newusers = new LoginController();
const users = new NewUsersController();

xdescribe("Login and Delete Current User", () => {
  let accessToken: string;
  let userId: number;

  before(`should log in a user`, async () => {
    let response = await newusers.loginuser("perma@gmail.com", "12345678");
    accessToken = response.body.token.accessToken.token;
    userId = response.body.user.id;

    checkStatusCode(response, 200);
    checkResponseTime(response, 300);
  });

  it(`should get current user details`, async () => {
    const response = await users.getCurrentUserDetails(accessToken);
    checkStatusCode(response, 200);
    const currentUser = response.body;
    expect(currentUser).to.have.property("id").to.be.equal(userId);
  });

  it(`should delete the current user`, async () => {
    const response = await users.deleteUserById(userId, accessToken);
    checkStatusCode(response, 204);
  });

  afterEach(() => {
    console.log("it was a test");
  });
});





