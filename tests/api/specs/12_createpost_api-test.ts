import { expect } from "chai";
import { PostsController } from "../lib/controllers/posts.controller";
import { checkStatusCode, checkResponseTime } from "../../helpers/functionsForChecking.helper";

const posts = new PostsController();

xdescribe("Posts controller", () => {
  it(`should create a new post`, async () => {
      let userData: object = {
      authorId: 0,
      previewImage: "string",
      body: "Lalala"
      };

    const response = await posts.createPost(userData)

    checkStatusCode(response, 200);
    checkResponseTime(response,500);

    const responseBody = response.body;

    expect(responseBody).to.have.property("id").that.is.a("number");
    expect(responseBody).to.have.property("createdAt").that.is.a("string");

    expect(responseBody).to.have.property("author");
    expect(responseBody.author).to.have.property("id").that.is.a("number");
    expect(responseBody.author).to.have.property("userName").that.is.equal("Iryna");

    expect(responseBody).to.have.property("comments").that.is.an("array");
    expect(responseBody).to.have.property("reactions").that.is.an("array");

    console.log(responseBody);
  });
});
