import { expect } from "chai";
import { UsersController } from "../lib/controllers/users.controller";
import { checkStatusCode, checkResponseTime } from "../../helpers/functionsForChecking.helper";

const users = new UsersController();

xdescribe("Users controller", () => {
  it(`should return an error when updating user with invalid data`, async () => {
    const userId = 3711; 
    const invalidUserData = {
      email: "",
    };

    const response = await users.updateCurrentUser(invalidUserData);

    checkStatusCode(response, 400);
    checkResponseTime(response,300);

  });

  it(`should return an error when deleting user with invalid ID`, async () => {
    const invalidUserId = 15000; 

    const response = await users.deleteUserById(invalidUserId);

    checkStatusCode(response,401);
    checkResponseTime(response,300);

  });

  it(`should return an error when getting user details with invalid ID`, async () => {
    const invalidUserId = 15687; 

    const response = await users.getUserById(invalidUserId);

    checkStatusCode(response, 404);
    checkResponseTime(response,300);

  });

});
