import { expect } from "chai";
import { PostsController } from "../lib/controllers/posts.controller";
import { checkStatusCode, checkResponseTime } from "../../helpers/functionsForChecking.helper";

const posts = new PostsController();

xdescribe("Posts controller", () => {

  it(`should return 400 when trying to create a comment with incorrect data`, async () => {
    const invalidCommentData = {
      authorId: 0, 
      postId: 0,  
      body: ""
    };

    const response = await posts.createComment(invalidCommentData);

    expect(response.statusCode, `Status Code should be 400`).to.be.equal(400);
  });

  it(`should return 404 when trying to add a reaction to a non-existent post`, async () => {
    const nonExistentPostId = "9876"; 
    const reactionData = {
      entityId: 9876,
      isLike: true,
      userId: 0,
    };

    const response = await posts.addReaction(reactionData);

    checkStatusCode(response, 404);
    checkResponseTime(response,500);
  });

  it(`should return 404 when trying to create a comment for a non-existent post`, async () => {
    const nonExistentPostId = "9876"; 
    const commentData = {
      authorId: 0,
      postId: 9876,
      body: "I non-existent"
    };

    const response = await posts.createComment(commentData);

    checkStatusCode(response, 404);
    checkResponseTime(response,500);
  });

  it(`should return 400 when trying to add an invalid reaction`, async () => {
    const invalidReactionData = {
      entityId: 0,
      isLike: "lol", 
      userId: 0,
    };

    const response = await posts.addReaction(invalidReactionData);

    checkStatusCode(response, 400);
    checkResponseTime(response,500);
  });

});
