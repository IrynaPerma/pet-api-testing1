import { expect } from "chai";
import { PostsController } from "../lib/controllers/posts.controller";
import { checkStatusCode, checkResponseTime } from "../../helpers/functionsForChecking.helper";

const posts = new PostsController();

xdescribe("Posts controller", () => {
  it(`should return all posts`, async () => {
    let response = await posts.getAllPosts();

    checkStatusCode(response, 200);
    checkResponseTime(response,700);

    console.log(response.body);
  });
});
