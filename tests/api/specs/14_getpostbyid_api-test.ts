import { expect } from "chai";
import { PostsController } from "../lib/controllers/posts.controller";
import { checkStatusCode, checkResponseTime } from "../../helpers/functionsForChecking.helper";

const posts = new PostsController();

xdescribe("Posts controller", () => {
  let postId: number; 

  it(`should return all posts`, async () => {
    let response = await posts.getAllPosts();

    checkStatusCode(response, 200);
    checkResponseTime(response,800);

    console.log(response.body[0]); 

    postId = response.body[0].id; 
  });

  it(`should return post by id`, async () => {
    let response = await posts.getPostById(postId);

    checkStatusCode(response, 200);
    checkResponseTime(response,800);
  });
});



// import { expect } from "chai";
// import { PostsController } from "../lib/controllers/posts.controller";

// const posts = new PostsController();

// describe("Posts controller", () => {
//   it(`should retrieve a post by its id`, async () => {

//     const getAllResponse = await posts.getAllPosts();
//     expect(getAllResponse.statusCode, `Status Code should be 200`).to.be.equal(200);
//     expect(getAllResponse.timings.phases.total, `Response time should be less than 8s`).to.be.lessThan(800);

//     const postId = 1631;

//     const response = await posts.getPostById(postId);

//     expect(response.statusCode, `Status Code should be 200`).to.be.equal(200);
//     expect(response.timings.phases.total, `Response time should be less than 15s`).to.be.lessThan(15000);

//     const responseBody = response.body;


//     expect(responseBody).to.have.property("id").that.is.equal(postId);
//     expect(responseBody).to.have.property("createdAt").that.is.a("string");
//     expect(responseBody).to.have.property("author");
//     expect(responseBody.author).to.have.property("id").that.is.a("number");
//     expect(responseBody.author).to.have.property("userName").that.is.equal("Iryna");
//     expect(responseBody).to.have.property("previewImage").that.is.a("string");
//     expect(responseBody).to.have.property("comments").that.is.an("array");
//     expect(responseBody).to.have.property("reactions").that.is.an("array");

//     console.log(responseBody);
//   });
// });




// import { expect } from "chai";
// import { PostsController } from "../lib/controllers/posts.controller";

// const posts = new PostsController();

// describe("Posts controller", () => {
//   it(`should retrieve a post by its id`, async () => {

//     const getAllResponse = await posts.getAllPosts();
//     expect(getAllResponse.statusCode, `Status Code should be 200`).to.be.equal(200);
//     expect(getAllResponse.timings.phases.total, `Response time should be less than 8s`).to.be.lessThan(800);

//     const id = 1633;

//     const responseGetPostById = await posts.getPostById(id); 
//     expect(responseGetPostById.statusCode, `Status Code should be 200`).to.be.equal(200);
//     expect(responseGetPostById.timings.phases.total, `Response time should be less than 15s`).to.be.lessThan(15000);

//     const responseBody = responseGetPostById.body;

//     expect(responseBody).to.have.property("id").that.is.equal(id);
//     expect(responseBody).to.have.property("createdAt").that.is.a("string");
//     expect(responseBody).to.have.property("author");
//     expect(responseBody.author).to.have.property("id").that.is.a("number");
//     expect(responseBody.author).to.have.property("userName").that.is.equal("Iryna");
//     expect(responseBody).to.have.property("previewImage").that.is.a("string");
//     expect(responseBody).to.have.property("body").that.is.a("string");
//     expect(responseBody).to.have.property("comments").that.is.an("array");
//     expect(responseBody).to.have.property("reactions").that.is.an("array");

//     console.log(responseBody);
//   });
// });

// import { expect } from "chai";
// import { PostsController } from "../lib/controllers/posts.controller";

// const posts = new PostsController();

// describe("Posts controller", () => {
//   let postId: string;

//   it(`should return all posts`, async () => {
//     let response = await posts.getAllPosts();

//     expect(response.statusCode, `Status Code should be 200`).to.be.equal(200);
//     expect(response.timings.phases.total, `Response time should be less than 8s`).to.be.lessThan(800);

//     console.log(response.body[1633]);

//     postId = response.body[1633].id
//   });

//   it(`should return post by id`, async () => {
//     let response = await posts.getPostById(postId);

//     expect(response.statusCode, `Status Code should be 200`).to.be.equal(200);
//     expect(response.timings.phases.total, `Response time should be less than 8s`).to.be.lessThan(800);
//   });
// });

