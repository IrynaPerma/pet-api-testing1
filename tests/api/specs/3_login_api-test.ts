import { expect } from "chai";
import { LoginController } from "../lib/controllers/login.controller";
import { checkStatusCode, checkResponseTime } from "../../helpers/functionsForChecking.helper";

const users = new LoginController();

xdescribe("Login controller", () => {
  it(`should log in a user`, async () => {
    let userData = {
      email: "iryna@gmail.com",
      password: "12345678",
    };

    const response = await users.loginUser(userData);
    checkStatusCode(response, 200);
    checkResponseTime(response,300);

    const responseBody = response.body;

    expect(responseBody.user.userName).to.equal("Iryna");
    expect(responseBody.user.email).to.equal("iryna@gmail.com");

    expect(responseBody.token.accessToken.token).to.be.a("string");
    expect(responseBody.token.accessToken.expiresIn).to.be.a("number");

    expect(responseBody.token.refreshToken).to.be.a("string");

    console.log(response.body);
  });
});









// import { expect } from "chai";
// import { UsersController } from "../lib/controllers/users.controller";
// import { AuthController } from "../lib/controllers/auth.controller";

// const users = new UsersController();
// const auth = new AuthController();

// xdescribe("Token usage", () => {
//     let accessToken: string;

//     before(`Login and get the token`, async () => {
//         let response = await auth.login("email.qa.test@gmail.com", "password");

//         accessToken = response.body.token.accessToken.token;
//         // console.log(accessToken);
//     });

//     it(`Usage is here`, async () => {
//         let userData: object = {
//             id: 7,
//             avatar: "string",
//             email: "alex.qa.test@gmail.com",
//             userName: "AlexQaNew",
//         };

//         let response = await users.updateUser(userData, accessToken);
//         expect(response.statusCode, `Status Code should be 204`).to.be.equal(204);
//     });
// });
