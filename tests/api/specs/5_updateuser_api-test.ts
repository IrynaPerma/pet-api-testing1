import { expect } from "chai";
import { UsersController } from "../lib/controllers/users.controller";
import { checkStatusCode, checkResponseTime } from "../../helpers/functionsForChecking.helper";

const users = new UsersController();

xdescribe("Users controller", () => {
  it(`should update the current user`, async () => {

    const userData = {
      id: 3708,
      avatar: "string",
      email: "irynka@gmail.com",
      userName: "Irynka"
    };

    const response = await users.updateCurrentUser(userData);

    checkStatusCode(response, 204);
    checkResponseTime(response,300);

    const responseBody = response.body;

  });
});
